package zipper

import (
	"github.com/mholt/archiver/v3"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
	"gitlab.com/evatix-go/pathhelper/pathjoin"

	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func ApplyCompress(compress *zipperinsfmt.Compress) *errorwrapper.Wrapper {
	if compress.IsInvalid() {
		return compress.GetInvalidError()
	}

	switch compress.Options.Format {
	case compressformats.Zip:
		return zipCompress(compress)
	case compressformats.Tar:
		return tarCompress(compress)
	case compressformats.TarGZ:
		return tarGZCompress(compress)
	case compressformats.TarXZ:
		return tarXZCompress(compress)
	case compressformats.TarBz2:
		return tarBz2Compress(compress)
	default:
		return errnew.Messages.Many(
			errtype.NotSupportedOption,
			compressformats.BasicEnumImpl.RangesInvalidMessage())
	}
}

func tarBz2Compress(compress *zipperinsfmt.Compress) *errorwrapper.Wrapper {
	tar := &archiver.Tar{
		OverwriteExisting:      compress.Overwrite.IsForceWrite,
		MkdirAll:               compress.RawConfig.IsMkdirAll,
		ImplicitTopLevelFolder: compress.RawConfig.IsImplicitTopLevelFolder,
		StripComponents:        compress.RawConfig.StripComponents,
		ContinueOnError:        compress.RawConfig.IsContinueOnError,
	}

	tarBz2 := archiver.TarBz2{
		Tar:              tar,
		CompressionLevel: compress.Options.CompressionLevel.ValueInt(),
	}

	err := tarBz2.Archive(
		compress.SourcePaths,
		pathjoin.JoinNormalized(
			compress.Destination.PrefixDir,
			compress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.CompressingFailed,
			"ApplyCompress -> archive to tar.bz2 failed\n"+compress.String(),
		)
	}

	return nil
}

func tarXZCompress(compress *zipperinsfmt.Compress) *errorwrapper.Wrapper {
	tar := &archiver.Tar{
		OverwriteExisting:      compress.Overwrite.IsForceWrite,
		MkdirAll:               compress.RawConfig.IsMkdirAll,
		ImplicitTopLevelFolder: compress.RawConfig.IsImplicitTopLevelFolder,
		StripComponents:        compress.RawConfig.StripComponents,
		ContinueOnError:        compress.RawConfig.IsContinueOnError,
	}

	tarXZ := archiver.TarXz{
		Tar: tar,
	}

	err := tarXZ.Archive(
		compress.SourcePaths,
		pathjoin.JoinNormalized(
			compress.Destination.PrefixDir,
			compress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.CompressingFailed,
			"ApplyCompress -> archive to tar.xz failed\n"+compress.String(),
		)
	}

	return nil
}

func tarGZCompress(compress *zipperinsfmt.Compress) *errorwrapper.Wrapper {
	tar := &archiver.Tar{
		OverwriteExisting:      compress.Overwrite.IsForceWrite,
		MkdirAll:               compress.RawConfig.IsMkdirAll,
		ImplicitTopLevelFolder: compress.RawConfig.IsImplicitTopLevelFolder,
		StripComponents:        compress.RawConfig.StripComponents,
		ContinueOnError:        compress.RawConfig.IsContinueOnError,
	}

	tarGZ := archiver.TarGz{
		Tar:              tar,
		CompressionLevel: compress.Options.CompressionLevel.ValueInt(),
		SingleThreaded:   compress.RawConfig.IsSingleThreadedCompress,
	}

	err := tarGZ.Archive(
		compress.SourcePaths,
		pathjoin.JoinNormalized(
			compress.Destination.PrefixDir,
			compress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.CompressingFailed,
			"ApplyCompress -> archive to tar.gz failed\n"+compress.String(),
		)
	}

	return nil
}

func tarCompress(compress *zipperinsfmt.Compress) *errorwrapper.Wrapper {
	tar := &archiver.Tar{
		OverwriteExisting:      compress.Overwrite.IsForceWrite,
		MkdirAll:               compress.RawConfig.IsMkdirAll,
		ImplicitTopLevelFolder: compress.RawConfig.IsImplicitTopLevelFolder,
		StripComponents:        compress.RawConfig.StripComponents,
		ContinueOnError:        compress.RawConfig.IsContinueOnError,
	}

	err := tar.Archive(
		compress.SourcePaths,
		pathjoin.JoinNormalized(
			compress.Destination.PrefixDir,
			compress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.CompressingFailed,
			"ApplyCompress -> archive to tar failed\n"+compress.String(),
		)
	}

	return nil
}

func zipCompress(compress *zipperinsfmt.Compress) *errorwrapper.Wrapper {
	zip := archiver.Zip{
		CompressionLevel:       compress.Options.CompressionLevel.ValueInt(),
		OverwriteExisting:      compress.Overwrite.IsForceWrite,
		MkdirAll:               compress.RawConfig.IsMkdirAll,
		SelectiveCompression:   compress.RawConfig.IsSelectiveCompression,
		ImplicitTopLevelFolder: compress.RawConfig.IsImplicitTopLevelFolder,
		StripComponents:        compress.RawConfig.StripComponents,
		ContinueOnError:        compress.RawConfig.IsContinueOnError,
	}

	err := zip.Archive(
		compress.SourcePaths,
		pathjoin.JoinNormalized(
			compress.Destination.PrefixDir,
			compress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.CompressingFailed,
			"ApplyCompress -> archive to zip failed\n"+compress.String(),
		)
	}

	return nil
}

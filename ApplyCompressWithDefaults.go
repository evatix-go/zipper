package zipper

import (
	"github.com/mholt/archiver/v3"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
	"gitlab.com/evatix-go/pathhelper/pathjoin"

	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func ApplyCompressWithDefaults(
	compressWithDefaults *zipperinsfmt.CompressWithDefaults,
) *errorwrapper.Wrapper {
	if compressWithDefaults.IsInvalid() {
		return compressWithDefaults.GetInvalidError()
	}

	err := archiver.
		Archive(
			compressWithDefaults.SourcePaths,
			pathjoin.JoinNormalized(
				compressWithDefaults.Destination.PrefixDir,
				compressWithDefaults.Destination.OutputDir,
			),
		)

	if err != nil {
		return errnew.Messages.Many(
			errtype.CompressingFailed,
			"ApplyCompressWithDefaults -> archive to default failed\n"+compressWithDefaults.String(),
		)
	}

	return nil
}

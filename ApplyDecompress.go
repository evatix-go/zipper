package zipper

import (
	"github.com/mholt/archiver/v3"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
	"gitlab.com/evatix-go/pathhelper/pathjoin"

	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func ApplyDecompress(decompress *zipperinsfmt.Decompress) *errorwrapper.Wrapper {
	if decompress.IsInvalid() {
		return decompress.GetInvalidError()
	}

	clearErr := removeExistingPathOnCondition(
		decompress.IsClearDir,
		decompress)

	if clearErr.HasError() {
		return clearErr
	}

	switch decompress.Options.Format {
	case compressformats.Zip:
		return zipDecompress(decompress)
	case compressformats.Tar:
		return tarDecompress(decompress)
	case compressformats.TarGZ:
		return tarGZDecompress(decompress)
	case compressformats.TarXZ:
		return tarXZDecompress(decompress)
	case compressformats.TarBz2:
		return tarBz2Decompress(decompress)
	default:
		return errnew.Messages.Many(
			errtype.NotSupportedOption,
			compressformats.BasicEnumImpl.RangesInvalidMessage())
	}
}

func zipDecompress(decompress *zipperinsfmt.Decompress) *errorwrapper.Wrapper {
	zip := archiver.Zip{
		OverwriteExisting:      decompress.Overwrite.IsForceWrite,
		MkdirAll:               decompress.RawConfig.IsMkdirAll,
		SelectiveCompression:   decompress.RawConfig.IsSelectiveCompression,
		ImplicitTopLevelFolder: decompress.RawConfig.IsImplicitTopLevelFolder,
		StripComponents:        decompress.RawConfig.StripComponents,
		ContinueOnError:        decompress.RawConfig.IsContinueOnError,
	}

	err := zip.Unarchive(
		decompress.SourcePath,
		pathjoin.JoinNormalized(
			decompress.Destination.PrefixDir,
			decompress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.DecompressingFailed,
			"ApplyDecompress -> decompress of zip failed\n"+decompress.String(),
		)
	}

	return nil
}

func tarDecompress(decompress *zipperinsfmt.Decompress) *errorwrapper.Wrapper {
	tar := &archiver.Tar{
		OverwriteExisting:      decompress.Overwrite.IsForceWrite,
		MkdirAll:               decompress.RawConfig.IsMkdirAll,
		ImplicitTopLevelFolder: decompress.RawConfig.IsImplicitTopLevelFolder,
		StripComponents:        decompress.RawConfig.StripComponents,
		ContinueOnError:        decompress.RawConfig.IsContinueOnError,
	}

	err := tar.Unarchive(
		decompress.SourcePath,
		pathjoin.JoinNormalized(
			decompress.Destination.PrefixDir,
			decompress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.DecompressingFailed,
			"ApplyDecompress -> decompress of tar failed\n"+decompress.String(),
		)
	}

	return nil
}

func tarGZDecompress(decompress *zipperinsfmt.Decompress) *errorwrapper.Wrapper {
	tar := archiver.NewTar()
	tarGZ := archiver.TarGz{
		Tar:            tar,
		SingleThreaded: decompress.RawConfig.IsSingleThreadedCompress,
	}

	err := tarGZ.Unarchive(
		decompress.SourcePath,
		pathjoin.JoinNormalized(
			decompress.Destination.PrefixDir,
			decompress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.DecompressingFailed,
			"ApplyDecompress -> decompress of tar.gz failed\n"+decompress.String(),
		)
	}

	return nil
}

func tarXZDecompress(decompress *zipperinsfmt.Decompress) *errorwrapper.Wrapper {
	tarXZ := archiver.NewTarXz()

	err := tarXZ.Unarchive(
		decompress.SourcePath,
		pathjoin.JoinNormalized(
			decompress.Destination.PrefixDir,
			decompress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.DecompressingFailed,
			"ApplyDecompress -> decompress of tar.xz failed\n"+decompress.String(),
		)
	}

	return nil
}

func tarBz2Decompress(decompress *zipperinsfmt.Decompress) *errorwrapper.Wrapper {
	tar := archiver.NewTar()
	tarBz2 := &archiver.TarBz2{
		Tar: tar,
	}

	err := tarBz2.Unarchive(
		decompress.SourcePath,
		pathjoin.JoinNormalized(
			decompress.Destination.PrefixDir,
			decompress.Destination.OutputDir,
		),
	)

	if err != nil {
		return errnew.Messages.Many(
			errtype.DecompressingFailed,
			"ApplyDecompress -> decompress of tar.bz2 failed\n"+decompress.String(),
		)
	}

	return nil
}

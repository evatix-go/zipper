package zipper

import (
	"gitlab.com/evatix-go/errorwrapper/errwrappers"

	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func ApplyDecompressUsingErrCollection(
	errCollection *errwrappers.Collection,
	decompress *zipperinsfmt.Decompress,
) (isSuccess bool) {
	if decompress == nil {
		return true
	}

	decompressErr := ApplyDecompress(decompress)
	errCollection.AddWrapperPtr(decompressErr)

	return decompressErr.IsSuccess()
}

package zipper

import (
	"github.com/mholt/archiver/v3"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
	"gitlab.com/evatix-go/pathhelper/pathjoin"

	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func ApplyDecompressWithDefaults(
	decompressWithDefaults *zipperinsfmt.DecompressWithDefaults,
) *errorwrapper.Wrapper {
	if decompressWithDefaults.IsInvalid() {
		return decompressWithDefaults.GetInvalidError()
	}

	err := archiver.
		Unarchive(
			decompressWithDefaults.SourcePath,
			pathjoin.JoinNormalized(
				decompressWithDefaults.Destination.PrefixDir,
				decompressWithDefaults.Destination.OutputDir,
			),
		)

	if err != nil {
		// return deferrwrapper.ArchiveCompression.
		// 	ConcatNewMessage(
		// 		"Decompress -> decompress to default failed.\n" +
		// 			decompressWithDefaults.String(),
		// 	)
		return errnew.Messages.Many(
			errtype.DecompressingFailed,
			"ApplyDecompressWithDefaults -> decompress to default failed\n"+decompressWithDefaults.String(),
		)
	}

	return nil
}

package zipper

import (
	"gitlab.com/evatix-go/errorwrapper/errwrappers"
	"gitlab.com/evatix-go/pathhelper/pathinsfmtexec/copyinsexec"
	"gitlab.com/evatix-go/pathhelper/pathinsfmtexec/downloadinsexec"
	"gitlab.com/evatix-go/pathhelper/pathinsfmtexec/pathmodifier"

	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func ApplyDownloadDecompress(
	downloadDecompress *zipperinsfmt.DownloadDecompress,
	errCollection *errwrappers.Collection,
) bool {
	if downloadDecompress == nil {
		return true
	}

	stateTracker := errCollection.StateTracker()

	if downloadDecompress.HasDownload() {
		downloadErr := downloadinsexec.Apply(downloadDecompress.Download)
		errCollection.AddWrapperPtr(downloadErr)
	}

	ApplyDecompressUsingErrCollection(
		errCollection,
		downloadDecompress.Decompress)

	if downloadDecompress.HasCopyPath() {
		copyPathErr := copyinsexec.Apply(
			downloadDecompress.CopyPath)
		errCollection.AddWrapperPtr(copyPathErr)
	}

	if downloadDecompress.HasPathModifiersApply() {
		pathmodifier.ApplyBaseUsingErrorCollection(
			false,
			errCollection,
			&downloadDecompress.BasePathModifiersApply,
		)
	}

	return stateTracker.IsSuccess()
}

package zipper

import (
	"gitlab.com/evatix-go/errorwrapper/errwrappers"

	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func ApplyDownloadDecompressInstructions(
	ins *zipperinsfmt.DownloadDecompressInstructions,
	errCollection *errwrappers.Collection,
) bool {
	if ins.IsEmpty() {
		return true
	}

	stateTracker := errCollection.StateTracker()
	for i := range ins.Instructions {
		ApplyDownloadDecompress(&ins.Instructions[i], errCollection)
	}

	return stateTracker.IsSuccess()
}

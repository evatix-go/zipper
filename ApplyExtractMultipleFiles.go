package zipper

import (
	"github.com/mholt/archiver/v3"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
	"gitlab.com/evatix-go/pathhelper/pathjoin"

	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func ExtractMultipleFiles(multipleFiles *zipperinsfmt.ExtractMultipleFiles) *errorwrapper.Wrapper {
	if multipleFiles.IsInvalid() {
		return multipleFiles.GetInvalidError()
	}

	switch multipleFiles.Options.Format {
	case compressformats.Zip:
		return zipExtractMultipleFiles(multipleFiles)
	case compressformats.Tar:
		return tarExtractMultipleFiles(multipleFiles)
	case compressformats.TarGZ:
		return tarGZExtractMultipleFiles(multipleFiles)
	case compressformats.TarXZ:
		return tarXZExtractMultipleFiles(multipleFiles)
	case compressformats.TarBz2:
		return tarBz2ExtractMultipleFiles(multipleFiles)
	default:
		return errnew.Messages.Many(
			errtype.NotSupportedOption,
			compressformats.BasicEnumImpl.RangesInvalidMessage())
	}
}

func zipExtractMultipleFiles(extractFiles *zipperinsfmt.ExtractMultipleFiles) *errorwrapper.Wrapper {
	zip := archiver.Zip{
		OverwriteExisting:      extractFiles.Overwrite.IsForceWrite,
		MkdirAll:               extractFiles.RawConfig.IsMkdirAll,
		SelectiveCompression:   extractFiles.RawConfig.IsSelectiveCompression,
		ImplicitTopLevelFolder: extractFiles.RawConfig.IsImplicitTopLevelFolder,
		StripComponents:        extractFiles.RawConfig.StripComponents,
		ContinueOnError:        extractFiles.RawConfig.IsContinueOnError,
	}

	for _, file := range extractFiles.SourcePaths {
		err := zip.Extract(
			extractFiles.SourcePath,
			file,
			pathjoin.JoinNormalized(
				extractFiles.Destination.PrefixDir,
				extractFiles.Destination.OutputDir,
			),
		)

		if err != nil {
			return errnew.Messages.Many(
				errtype.DecompressingIssue,
				"ExtractMultipleFiles -> extraction of "+file+" failed.\n"+extractFiles.String(),
			)
		}
	}

	return nil
}

func tarExtractMultipleFiles(extractFiles *zipperinsfmt.ExtractMultipleFiles) *errorwrapper.Wrapper {
	tar := archiver.Tar{
		OverwriteExisting:      extractFiles.Overwrite.IsForceWrite,
		MkdirAll:               extractFiles.RawConfig.IsMkdirAll,
		ImplicitTopLevelFolder: extractFiles.RawConfig.IsImplicitTopLevelFolder,
		StripComponents:        extractFiles.RawConfig.StripComponents,
		ContinueOnError:        extractFiles.RawConfig.IsContinueOnError,
	}

	for _, file := range extractFiles.SourcePaths {
		err := tar.Extract(
			extractFiles.SourcePath,
			file,
			pathjoin.JoinNormalized(
				extractFiles.Destination.PrefixDir,
				extractFiles.Destination.OutputDir,
			),
		)

		if err != nil {
			return errnew.Messages.Many(
				errtype.DecompressingIssue,
				"ExtractMultipleFiles -> extraction of "+file+" failed.\n"+extractFiles.String(),
			)
		}
	}

	return nil
}

func tarGZExtractMultipleFiles(extractFiles *zipperinsfmt.ExtractMultipleFiles) *errorwrapper.Wrapper {
	tarGZ := archiver.TarGz{
		SingleThreaded: extractFiles.RawConfig.IsSingleThreadedCompress,
	}

	for _, file := range extractFiles.SourcePaths {
		err := tarGZ.Extract(
			extractFiles.SourcePath,
			file,
			pathjoin.JoinNormalized(
				extractFiles.Destination.PrefixDir,
				extractFiles.Destination.OutputDir,
			),
		)

		if err != nil {
			return errnew.Messages.Many(
				errtype.DecompressingIssue,
				"ExtractMultipleFiles -> extraction of "+file+" failed.\n"+extractFiles.String(),
			)
		}
	}

	return nil
}

func tarXZExtractMultipleFiles(extractFiles *zipperinsfmt.ExtractMultipleFiles) *errorwrapper.Wrapper {
	tarXZ := archiver.TarXz{}

	for _, file := range extractFiles.SourcePaths {
		err := tarXZ.Extract(
			extractFiles.SourcePath,
			file,
			pathjoin.JoinNormalized(
				extractFiles.Destination.PrefixDir,
				extractFiles.Destination.OutputDir,
			),
		)

		if err != nil {
			return errnew.Messages.Many(
				errtype.DecompressingIssue,
				"ExtractMultipleFiles -> extraction of "+file+" failed.\n"+extractFiles.String(),
			)
		}
	}

	return nil
}

func tarBz2ExtractMultipleFiles(extractFiles *zipperinsfmt.ExtractMultipleFiles) *errorwrapper.Wrapper {
	tarBz2 := archiver.TarBz2{}

	for _, file := range extractFiles.SourcePaths {
		err := tarBz2.Extract(
			extractFiles.SourcePath,
			file,
			pathjoin.JoinNormalized(
				extractFiles.Destination.PrefixDir,
				extractFiles.Destination.OutputDir,
			),
		)

		if err != nil {
			return errnew.Messages.Many(
				errtype.DecompressingIssue,
				"ExtractMultipleFiles -> extraction of "+file+" failed.\n"+extractFiles.String(),
			)
		}
	}

	return nil
}

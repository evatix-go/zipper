package zipper

import (
	"github.com/mholt/archiver/v3"
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/core/coredata/corestr"
	"gitlab.com/evatix-go/errorwrapper/errdata/errstr"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"

	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func ApplyListFiles(listFiles *zipperinsfmt.ListFiles) *errstr.Collection {
	if listFiles.IsInvalid() {
		return &errstr.Collection{
			Collection:   nil,
			ErrorWrapper: listFiles.GetInvalidError(),
		}
	}

	switch listFiles.Options.Format {
	case compressformats.Zip:
		return zipListFiles(listFiles)
	case compressformats.Tar:
		return tarListFiles(listFiles)
	case compressformats.TarGZ:
		return tarGZListFiles(listFiles)
	case compressformats.TarXZ:
		return tarXZListFiles(listFiles)
	case compressformats.TarBz2:
		return tarBz2ListFiles(listFiles)
	default:
		errWp := errnew.Messages.Many(
			errtype.NotSupportedOption,
			compressformats.BasicEnumImpl.RangesInvalidMessage())

		return errstr.New.Collection.ErrorWrapper(errWp)
	}
}

func zipListFiles(files *zipperinsfmt.ListFiles) *errstr.Collection {
	strCollection := corestr.New.Collection.Cap(constants.ArbitraryCapacity10)
	err := archiver.DefaultZip.Walk(
		files.SourcePath,
		func(f archiver.File) error {
			strCollection.Add(f.Name())
			return nil
		})

	if err != nil {
		return &errstr.Collection{
			Collection: nil,
			ErrorWrapper: errnew.Messages.Many(
				errtype.FileAccess,
				"ApplyListFiles -> listing of zip failed.\n"+
					files.String(),
			),
		}
	}

	return &errstr.Collection{
		Collection:   strCollection,
		ErrorWrapper: nil,
	}
}

func tarListFiles(files *zipperinsfmt.ListFiles) *errstr.Collection {
	strCollection := corestr.New.Collection.Cap(constants.ArbitraryCapacity10)
	err := archiver.DefaultTar.Walk(
		files.SourcePath,
		func(f archiver.File) error {
			strCollection.Add(f.Name())
			return nil
		})

	if err != nil {
		return &errstr.Collection{
			Collection: nil,
			ErrorWrapper: errnew.Messages.Many(
				errtype.FileAccess,
				"ApplyListFiles -> listing of tar failed.\n"+
					files.String(),
			),
		}
	}

	return &errstr.Collection{
		Collection:   strCollection,
		ErrorWrapper: nil,
	}
}

func tarGZListFiles(files *zipperinsfmt.ListFiles) *errstr.Collection {
	strCollection := corestr.New.Collection.Cap(constants.ArbitraryCapacity10)
	err := archiver.DefaultTarGz.Walk(
		files.SourcePath,
		func(f archiver.File) error {
			strCollection.Add(f.Name())
			return nil
		})

	if err != nil {
		return &errstr.Collection{
			Collection: nil,
			ErrorWrapper: errnew.Messages.Many(
				errtype.FileAccess,
				"ApplyListFiles -> listing of tar.gz failed.\n"+
					files.String(),
			),
		}
	}

	return &errstr.Collection{
		Collection:   strCollection,
		ErrorWrapper: nil,
	}
}

func tarXZListFiles(files *zipperinsfmt.ListFiles) *errstr.Collection {
	strCollection := corestr.New.Collection.Cap(constants.ArbitraryCapacity10)
	err := archiver.DefaultTarXz.Walk(
		files.SourcePath,
		func(f archiver.File) error {
			strCollection.Add(f.Name())
			return nil
		})

	if err != nil {
		return &errstr.Collection{
			Collection: nil,
			ErrorWrapper: errnew.Messages.Many(
				errtype.FileAccess,
				"ApplyListFiles -> listing of tar.xz failed.\n"+
					files.String(),
			),
		}
	}

	return &errstr.Collection{
		Collection:   strCollection,
		ErrorWrapper: nil,
	}
}

func tarBz2ListFiles(files *zipperinsfmt.ListFiles) *errstr.Collection {
	strCollection := corestr.New.Collection.Cap(constants.ArbitraryCapacity10)
	err := archiver.DefaultTarBz2.Walk(
		files.SourcePath,
		func(f archiver.File) error {
			strCollection.Add(f.Name())
			return nil
		})

	if err != nil {
		return &errstr.Collection{
			Collection: nil,
			ErrorWrapper: errnew.Messages.Many(
				errtype.FileAccess,
				"ApplyListFiles -> listing of tar.bz2 failed.\n"+
					files.String(),
			),
		}
	}

	return &errstr.Collection{
		Collection:   strCollection,
		ErrorWrapper: nil,
	}
}

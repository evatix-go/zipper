<!-- ![Use Package logo](UseLogo) -->

# Zipper Intro

zipper package executes zipping related functionalities

## Git Clone

`git clone https://gitlab.com/evatix-go/zipper.git`

### 2FA enabled, for linux

`git clone https://[YourGitLabUserName]:[YourGitlabAcessTokenGenerateFromGitlabsTokens]@gitlab.com/evatix-go/zipper.git`

### Prerequisites

- Update git to latest 2.29
- Update or install the latest of Go 1.15.2
- Either add your ssh key to your gitlab account
- Or, use your access token to clone it.

## Installation

`go get gitlab.com/evatix-go/zipper`

### Go get issue for private package

- Update git to 2.29
- Enable go modules. (Windows : `go env -w GO111MODULE=on`, Unix : `export GO111MODULE=on`)
- Add `gitlab.com/evatix-go` to go env private

To set for Windows:

`go env -w GOPRIVATE=[AddExistingOnes;]gitlab.com/evatix-go`

To set for Unix:

`expoort GOPRIVATE=[AddExistingOnes;]gitlab.com/evatix-go`

## Why `zipper`

To zip/unzip files and folders

## Examples

- N/A

## Acknowledgement

- [Archiver](https://github.com/mholt/archiver)

## Links

- [GitMind](https://gitmind.com/app/doc/83bfe6b653e44a685f6db18e182733f0)

## Issues

- [Create your issues](https://gitlab.com/evatix-go/zipper/-/issues)

## Notes

- N/A

## Contributors

## License

[Evatix MIT License](/LICENSE)

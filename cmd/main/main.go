package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/mholt/archiver/v3"
	"gitlab.com/evatix-go/core/chmodhelper/chmodins"
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/core/filemode"
	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/enum/compresslevels"
	"gitlab.com/evatix-go/errorwrapper/errwrappers"
	"gitlab.com/evatix-go/pathhelper/createdir"
	"gitlab.com/evatix-go/pathhelper/pathinsfmt"
	"gitlab.com/evatix-go/pathhelper/pathjoin"
	"gitlab.com/evatix-go/zipper"
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func main() {
	// fmt.Println("Hello World")
	// zipArchive()
	// zipExtract()
	// zipUnArchive()

	// tarArchive()
	// tarUnArchive()

	// tarGZArchive()
	// tarGZUnArchive()

	// tarXZArchive()
	// tarXZUnArchive()

	// compressTest()

	// applyCompressTest()

	// tarXZArchive()

	applyDownloadDecompressTest()
}

func applyDownloadDecompressTest() {
	createdir.RemoveCreateAll(
		true,
		false,
		true,
		true,
		defaultRunDir,
		filemode.DirDefault)

	zipFileName := "cimux.zip"
	zipOutDirName := "zip-out"
	zipOutDirPath := pathjoin.JoinNormalized(
		defaultRunDir, zipOutDirName)

	zipCopiedDirPath := pathjoin.JoinNormalized(
		defaultRunDir,
		"zip-copied")

	// https://gitlab.com/evatix-go/public-content/uploads/21708626d1899371373cb5b8e70a724e/cimux-ins.zip
	ins := &zipperinsfmt.DownloadDecompress{
		Download: &pathinsfmt.Download{
			Url:              "https://gitlab.com/evatix-go/public-content/uploads/21708626d1899371373cb5b8e70a724e/cimux-ins.zip",
			Destination:      defaultRunDir,
			FileName:         zipFileName,
			ParallelRequests: constants.Capacity2,
			MaxRetries:       constants.Capacity2,
			IsCreateDir:      true,
			IsClearDir:       true,
			IsNormalizePath:  true,
			IsSkipOnExist:    false,
		},
		Decompress: &zipperinsfmt.Decompress{
			SourcePath: pathjoin.JoinNormalized(defaultRunDir, zipFileName),
			IsClearDir: true,
			Destination: &zipperinsfmt.Destination{
				PrefixDir: pathjoin.JoinNormalized(defaultRunDir, "dest"),
				OutputDir: zipOutDirName,
			},
			Options: &zipperinsfmt.Options{
				Format:           compressformats.Zip,
				CompressionLevel: compresslevels.Best,
			},
			Overwrite: &zipperinsfmt.Overwrite{
				IsForceWrite:       true,
				IsSkipOnExistFiles: false,
			},
			RawConfig: &zipperinsfmt.RawConfig{
				IsMkdirAll: true,
			},
		},
		CopyPath: &pathinsfmt.CopyPath{
			SourceDestinationPlusCompiled: pathinsfmt.SourceDestinationPlusCompiled{
				BaseSourceDestination: pathinsfmt.BaseSourceDestination{
					Source:      zipOutDirPath,
					Destination: zipCopiedDirPath,
				},
			},
			Options: &pathinsfmt.CopyPathOptions{
				IsRecursive: true,
			},
		},
		IsApplyPathsChmod: true,
		BasePathModifiersApply: pathinsfmt.BasePathModifiersApply{
			PathModifiersApply: &pathinsfmt.PathModifiersApply{
				BaseGenericPathsCollection: pathinsfmt.BaseGenericPathsCollection{
					GenericPathsCollection: &pathinsfmt.GenericPathsCollection{
						AllDiffPaths: []pathinsfmt.AllDiffPaths{
							{
								Paths: []string{
									zipCopiedDirPath,
								},
								IsNormalizeApply: true,
							},
						},
					},
				},
				PathModifiers: []pathinsfmt.PathModifier{
					{
						BaseRwxInstructions: chmodins.BaseRwxInstructions{
							RwxInstructions: []chmodins.RwxInstruction{
								{
									RwxOwnerGroupOther: chmodins.RwxOwnerGroupOther{
										Owner: "rwx",
										Group: "rwx",
										Other: "rwx",
									},
								},
							},
						},
					},
				},
			},
		},
	}

	errCollection := errwrappers.Empty()
	zipper.ApplyDownloadDecompress(ins, errCollection)
	errCollection.HandleError()
}

func applyCompressTest() {
	ins := &zipperinsfmt.Compress{
		SourcePaths: []string{
			"./assets/json-formats/Compress.json",
			"./assets/json-formats/Decompress.json",
		},
		Destination: &zipperinsfmt.Destination{
			PrefixDir: "./xxxxy",
			OutputDir: "assets1.tar.gz",
		},
		Options: &zipperinsfmt.Options{
			Format:           compressformats.TarGZ,
			CompressionLevel: compresslevels.Best,
		},
		Overwrite: &zipperinsfmt.Overwrite{
			IsForceWrite:       true,
			IsSkipOnExistFiles: false,
		},
		RawConfig: &zipperinsfmt.RawConfig{
			IsMkdirAll: true,
		},
	}

	errW := zipper.ApplyCompress(ins)
	errW.HandleError()
}

func compressTest() {
	jsonFile, err := os.Open("assets/json-formats/Compress.json")
	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully Opened Compress.json")

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var result zipperinsfmt.Compress
	errUnmarshal := json.Unmarshal(byteValue, &result)
	if errUnmarshal != nil {
		panic(errUnmarshal)
	}

	fmt.Println(result)
	errW := zipper.ApplyCompress(&result)
	errW.HandleError()
}

func bz2Compress(in io.Reader, out io.Writer) {
	bz2 := archiver.NewBz2()
	err := bz2.Compress(in, out)
	if err != nil {
		panic(err)
	}
}

func bz2DeCompress(in io.Reader, out io.Writer) {
	bz2 := archiver.NewBz2()
	err := bz2.Decompress(in, out)
	if err != nil {
		panic(err)
	}
}

func tarXZArchive() {
	tar := archiver.NewTarXz()
	err := tar.Archive([]string{"./assets", "./go.mod"}, "test.tar.xz")
	if err != nil {
		panic(err)
	}
}

func tarXZUnArchive() {
	tar := archiver.NewTarXz()
	err := tar.Unarchive("test.tar.xz", "testasset")
	if err != nil {
		panic(err)
	}
}

func tarGZArchive() {
	tar := archiver.NewTarGz()
	err := tar.Archive([]string{"./assets", "./go.mod"}, "test.tar.gz")
	if err != nil {
		panic(err)
	}
}

func tarGZUnArchive() {
	tar := archiver.NewTarGz()
	err := tar.Unarchive("test.tar.gz", "testasset")
	if err != nil {
		panic(err)
	}
}

func tarArchive() {
	tar := archiver.NewTar()
	err := tar.Archive([]string{"./assets", "./go.mod"}, "test.tar")
	if err != nil {
		panic(err)
	}
}

func tarUnArchive() {
	tar := archiver.NewTar()
	err := tar.Unarchive("test.tar", "testasset")
	if err != nil {
		panic(err)
	}
}

func zipArchive() {
	zip := archiver.NewZip()
	zip.CompressionLevel = 0
	err := zip.Archive([]string{"Decompress.go", "ApplyCompress.go", "makefile"}, "test.zip")
	if err != nil {
		panic(err)
	}
}

func zipExtract() {
	zip := archiver.NewZip()
	srcFiles := []string{"Decompress.go"}

	for _, src := range srcFiles {
		err := zip.Extract("test.zip", src, "./output")
		if err != nil {
			panic(err)
		}
	}
}

func zipUnArchive() {
	zip := archiver.NewZip()
	err := zip.Unarchive("test.zip", "testasset")
	if err != nil {
		panic(err)
	}
}

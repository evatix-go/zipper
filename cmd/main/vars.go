package main

import (
	"gitlab.com/evatix-go/core/codestack"
	"gitlab.com/evatix-go/pathhelper/pathjoin"
)

var (
	pkgName       = codestack.PackageName()
	defaultRunDir = pathjoin.WithTempPlusDefaults(
		pkgName,
		"testing-dir")
)

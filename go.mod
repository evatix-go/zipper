module gitlab.com/evatix-go/zipper

go 1.17

require (
	github.com/codingsince1985/checksum v1.1.0
	github.com/mholt/archiver/v3 v3.5.1
	github.com/smartystreets/goconvey v1.7.2
	gitlab.com/evatix-go/core v1.3.55
	gitlab.com/evatix-go/enum v0.4.2
	gitlab.com/evatix-go/errorwrapper v1.1.5
	gitlab.com/evatix-go/pathhelper v0.7.8
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/dsnet/compress v0.0.2-0.20210315054119-f66993602bf5 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/klauspost/compress v1.15.1 // indirect
	github.com/klauspost/pgzip v1.2.5 // indirect
	github.com/nwaples/rardecode v1.1.3 // indirect
	github.com/pierrec/lz4/v4 v4.1.14 // indirect
	github.com/smartystreets/assertions v1.2.0 // indirect
	github.com/ulikunitz/xz v0.5.10 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	golang.org/x/sys v0.0.0-20220330033206-e17cdc41300f // indirect
)

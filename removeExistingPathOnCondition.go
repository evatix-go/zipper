package zipper

import (
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/pathhelper/deletepaths"
	"gitlab.com/evatix-go/pathhelper/pathjoin"
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func removeExistingPathOnCondition(
	isClear bool,
	decompress *zipperinsfmt.Decompress,
) *errorwrapper.Wrapper {
	if !isClear {
		return nil
	}

	finalPath := pathjoin.JoinNormalized(
		decompress.Destination.PrefixDir,
		decompress.Destination.OutputDir,
	)

	return deletepaths.RecursiveOnExist(finalPath)
}

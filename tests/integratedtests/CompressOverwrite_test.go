package integratedtests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/core/constants"

	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/zipper"
	"gitlab.com/evatix-go/zipper/tests/testwrappers/compresstestwrappers"
)

// scenario 3 // only applicable for zip & tar
func Test_Compress_Force_OverWrite(t *testing.T) {
	for _, testCase := range compresstestwrappers.CompressTestCases {
		Convey("Overwrite : "+testCase.PackageName+"->"+testCase.MethodName+"->"+testCase.CaseHeader, t, func() {
			for _, compress := range *testCase.InputData {
				if (compress.Options.Format == compressformats.Zip ||
					compress.Options.Format == compressformats.Tar) &&
					compress.Overwrite.IsForceWrite {
					Convey("ApplyCompress : "+compress.Destination.OutputDir, func() {
						compressErrWrapper := zipper.ApplyCompress(compress)
						So(compressErrWrapper.HasError(), ShouldNotEqual, true)

						Convey("Modify & Overwrite", func() {
							compress.SourcePaths = append(
								compress.SourcePaths,
								"./assets/json-formats/ListFiles.json",
							)

							compressErrWrapper := zipper.ApplyCompress(compress)
							compressErrWrapper.HandleError()
							So(compressErrWrapper.HasError(), ShouldNotEqual, true)

							Convey("Decompressing with defaults", func() {
								decompress := prepareDecompressFromCompress(compress)
								decompressErrWrapper := zipper.ApplyDecompress(decompress)
								So(decompressErrWrapper.HasError(), ShouldNotEqual, true)

								Convey("Checksum of source files and decompressed directory", func() {
									md5ForMultipleFiles := multipleFilesMd5(compress.SourcePaths)
									So(
										md5ForMultipleFiles.ErrorWrapper.HasError(),
										ShouldNotEqual,
										true,
									)

									md5ForDirectory := directoryMd5(
										TempDirectory +
											constants.ForwardSlash +
											compress.Options.Format.String(),
									)

									So(md5ForDirectory.ErrorWrapper.HasError(), ShouldNotEqual, true)
									So(md5ForDirectory.Value, ShouldEqual, md5ForMultipleFiles.Value)
								})
							})
						})
					})
				}
			}
		})
	}
}

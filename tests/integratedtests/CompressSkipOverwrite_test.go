package integratedtests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/core/constants"

	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/zipper"
	"gitlab.com/evatix-go/zipper/internal/strhelperinternal"
	"gitlab.com/evatix-go/zipper/tests/testwrappers/compresstestwrappers"
)

// scenario 4 // only applicable for zip & tar
func Test_Compress_Skip_OverWrite(t *testing.T) {
	var beforeModifyChecksumHashes []string
	var afterModifyChecksumHashes []string
	var decompressedChecksumHashes []string
	var changedFileHash string
	for _, testCase := range compresstestwrappers.CompressTestCases {
		Convey("Skip If Exist : "+testCase.PackageName+"->"+testCase.MethodName+"->"+testCase.CaseHeader, t, func() {
			for _, compress := range *testCase.InputData {
				if (compress.Options.Format == compressformats.Zip ||
					compress.Options.Format == compressformats.Tar) &&
					compress.Overwrite.IsSkipOnExistFiles {
					Convey("ApplyCompress", func() {
						compressErrWrapper := zipper.ApplyCompress(compress)
						So(compressErrWrapper.HasError(), ShouldNotEqual, true)
					})

					Convey("CheckSum Of Source", func() {
						results := multipleFilesMd5Array(compress.SourcePaths)

						beforeModifyChecksumHashes = results.Items()
					})

					Convey("Modify & Store Changed File Hash", func() {
						compress.SourcePaths = append(
							compress.SourcePaths,
							ReadMe,
						)

						result := singleFileMd5(ReadMe)
						So(result.ErrorWrapper.HasError(), ShouldNotEqual, true)
						changedFileHash = result.Value

						results := multipleFilesMd5Array(
							compress.SourcePaths,
						)

						afterModifyChecksumHashes = results.Items()
					})

					Convey("Decompressing with defaults", func() {
						decompress := prepareDecompressFromCompress(compress)
						decompressErrWrapper := zipper.ApplyDecompress(decompress)
						So(decompressErrWrapper.HasError(), ShouldNotEqual, true)
					})

					Convey("Checksum of source files before modification and decompressed directory", func() {
						md5ForDirectory := directoryMd5Array(
							TempDirectory +
								constants.ForwardSlash +
								compress.Options.Format.String(),
						)

						decompressedChecksumHashes = md5ForDirectory.Items()
						So(beforeModifyChecksumHashes, ShouldEqual, decompressedChecksumHashes)
					})

					Convey("Checking if modified file hash is missing from decompressed Directory Hashes", func() {
						missingElement := strhelperinternal.MissingElement(
							afterModifyChecksumHashes,
							decompressedChecksumHashes)

						So(missingElement, ShouldEqual, changedFileHash)
					})
				}
			}
		})
	}
}

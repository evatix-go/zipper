package integratedtests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/core/constants"

	"gitlab.com/evatix-go/zipper"
	"gitlab.com/evatix-go/zipper/tests/testwrappers/compresstestwrappers"
)

func Test_Compress(t *testing.T) {
	for _, testCase := range compresstestwrappers.CompressTestCases {
		Convey(testCase.PackageName+"->"+testCase.MethodName+"->"+testCase.CaseHeader, t, func() {
			// scenario 1 -> for every testcases (testcases include all formats)
			for _, compress := range *testCase.InputData {
				Convey("Compressing :"+compress.Destination.OutputDir, func() {
					compressErrWrapper := zipper.ApplyCompress(compress)
					compressErrWrapper.HandleError()
					So(compressErrWrapper.HasError(), ShouldNotEqual, true)

					Convey("Decompressing with defaults : "+compress.Destination.OutputDir, func() {
						decompress := prepareDecompressFromCompress(compress)
						decompressErrWrapper := zipper.ApplyDecompress(decompress)
						decompressErrWrapper.HandleError()
						So(decompressErrWrapper.HasError(), ShouldNotEqual, true)

						Convey("Checksum of source files and decompressed directory", func() {
							md5ForMultipleFiles := multipleFilesMd5(compress.SourcePaths)
							So(
								md5ForMultipleFiles.ErrorWrapper.HasError(),
								ShouldNotEqual,
								true,
							)

							md5ForDirectory := directoryMd5(
								TempDirectory +
									constants.ForwardSlash +
									compress.Options.Format.String(),
							)

							So(md5ForDirectory.ErrorWrapper.HasError(), ShouldNotEqual, true)
							So(md5ForDirectory.Value, ShouldEqual, md5ForMultipleFiles.Value)
						})
					})
				})
			}
		})
	}
}

package integratedtests

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/codingsince1985/checksum"
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/errorwrapper/errdata/errstr"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

func directoryMd5(directory string) *errstr.Result {
	md5Arr := make([]string, constants.Zero, constants.ArbitraryCapacity6)

	err := filepath.Walk(
		directory,
		func(path string, info os.FileInfo, err error) error {
			if path != directory {
				result := singleFileMd5(path)
				if result.ErrorWrapper.HasError() {
					return result.ErrorWrapper.Error()
				}

				md5Arr = append(md5Arr, result.Value)
			}

			return nil
		},
	)

	if err != nil {
		errWp := errnew.Path.Messages(
			errtype.Hash,
			err.Error(),
			directory,
			"directoryMd5 -> failed to generate hash for directory")

		return errstr.New.Result.ErrorWrapper(errWp)
	}

	hashStrings := ""
	for _, str := range md5Arr {
		hashStrings += str
	}

	md5Hash, err2 := checksum.MD5sumReader(
		strings.NewReader(hashStrings))

	if err2 != nil {
		errWp := errnew.Path.Messages(
			errtype.Hash,
			err2.Error(),
			directory,
			"directoryMd5 -> failed to generate hash for directory")

		return errstr.New.Result.ErrorWrapper(errWp)
	}

	return errstr.New.Result.ValueOnly(md5Hash)
}

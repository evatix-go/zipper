package integratedtests

import (
	"os"
	"path/filepath"

	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/core/coredata/corestr"
)

func directoryMd5Array(directory string) *corestr.Collection {
	collection := corestr.New.Collection.Cap(constants.Ten)

	err := filepath.Walk(
		directory,
		func(path string, info os.FileInfo, err error) error {
			if path != directory {
				result := singleFileMd5(path)
				result.ErrorWrapper.HandleError()

				// md5Arr = append(md5Arr, result.Value)
				collection.Add(result.Value)
			}

			return nil
		},
	)

	if err != nil {
		panic(err)
	}

	return collection
}

package downloaddecompresstests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/pathhelper/pathinsfmtexec/downloadinsexec"
)

func Test_ApplyDownload(t *testing.T) {
	errW := downloadinsexec.Apply(nil)

	Convey(
		"If no download instruction given the Apply should do noting and return empty errorwrapper",
		t,
		func() {
			So(errW.HasError(), ShouldBeFalse)
		},
	)
}

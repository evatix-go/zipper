package downloaddecompresstests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/errorwrapper/errwrappers"
	"gitlab.com/evatix-go/pathhelper/checksummer"
	"gitlab.com/evatix-go/pathhelper/hashas"

	"gitlab.com/evatix-go/zipper"
	"gitlab.com/evatix-go/zipper/tests/testwrappers/downloaddecompresstestwrappers"
)

func Test_CopyVerify(t *testing.T) {
	// 0. Setup
	ins := getDownloadDecompress()
	errCollection := errwrappers.Empty()
	zipper.ApplyDownloadDecompress(ins, errCollection)
	errCollection.HandleError()

	// 1. Arrange
	srcSums := checksummer.NewSync(
		true,
		downloaddecompresstestwrappers.SourceToDecompressedFolder,
		hashas.Md5,
	)

	dstSums := checksummer.NewSync(
		true,
		downloaddecompresstestwrappers.CopyTo,
		hashas.Md5,
	)

	// 2. Act
	errC := srcSums.VerifyError(true, true, dstSums)

	// 3. Assert
	Convey("Apply Download Decompress -> Copy Path", t, func() {
		Convey("Should Verify Error", func() {
			So(errC.HasError(), ShouldBeFalse)
		})

		Convey("Should Return Zero Error", func() {
			So(errC.Length(), ShouldEqual, constants.Zero)
		})
	})
}

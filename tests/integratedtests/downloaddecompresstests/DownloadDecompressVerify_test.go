package downloaddecompresstests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/errorwrapper/errverify"
	"gitlab.com/evatix-go/errorwrapper/errwrappers"

	"gitlab.com/evatix-go/zipper"
	"gitlab.com/evatix-go/zipper/tests/testwrappers/downloaddecompresstestwrappers"
)

func Test_DownloadDecompress_Verify(t *testing.T) {
	for i, testCase := range downloaddecompresstestwrappers.DownloadDecompressVerifierTestCases {
		// Arrange

		errCollection := errwrappers.Empty()

		// Act
		zipper.ApplyDownloadDecompress(
			testCase.Instruction, errCollection)

		errVerifyParams := &errverify.VerifyCollectionParams{
			CaseIndex:       i,
			FuncName:        testCase.Verifier.FunctionName,
			TestCaseName:    testCase.Verifier.Header,
			ErrorCollection: errCollection,
		}

		// Assert
		Convey(testCase.Header, t, func() {
			isSuccess := testCase.Verifier.IsMatch(errVerifyParams)

			So(isSuccess, ShouldBeTrue)
		})
	}
}

package downloaddecompresstests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/core/chmodhelper"
	"gitlab.com/evatix-go/errorwrapper/errwrappers"

	"gitlab.com/evatix-go/zipper"
	"gitlab.com/evatix-go/zipper/tests/testwrappers/downloaddecompresstestwrappers"
)

func Test_PathModifiers(t *testing.T) {
	// 0. Setup
	ins := getDownloadDecompress()
	errCollection := errwrappers.Empty()
	zipper.ApplyDownloadDecompress(ins, errCollection)
	errCollection.HandleError()

	// 1. Arrange
	locations := []string{downloaddecompresstestwrappers.CopyTo}
	rwx := downloaddecompresstestwrappers.RWXAll
	// 2. Act
	err := chmodhelper.ChmodVerify.PathsUsingPartialRwxOptions(
		true,
		true,
		rwx,
		locations...,
	)

	// 3. Assert
	Convey("Apply Download Decompress -> Path Modifiers", t, func() {
		Convey("Should Verify Error", func() {
			So(err, ShouldBeNil)
		})
	})
}

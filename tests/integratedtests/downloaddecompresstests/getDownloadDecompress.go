package downloaddecompresstests

import (
	"gitlab.com/evatix-go/core/chmodhelper/chmodins"
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/pathhelper/pathinsfmt"

	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/enum/compresslevels"
	"gitlab.com/evatix-go/zipper/tests/testwrappers/downloaddecompresstestwrappers"
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func getDownloadDecompress() *zipperinsfmt.DownloadDecompress {
	return &zipperinsfmt.DownloadDecompress{
		Download: &pathinsfmt.Download{
			Url:              downloaddecompresstestwrappers.Url,
			Destination:      downloaddecompresstestwrappers.DownloadDir,
			FileName:         downloaddecompresstestwrappers.ZipFileName,
			ParallelRequests: constants.Capacity2,
			MaxRetries:       constants.Capacity2,
			IsCreateDir:      true,
			IsClearDir:       true,
			IsNormalizePath:  true,
			IsSkipOnExist:    false,
		},
		Decompress: &zipperinsfmt.Decompress{
			SourcePath: downloaddecompresstestwrappers.SourceToZip,
			IsClearDir: true,
			Destination: &zipperinsfmt.Destination{
				PrefixDir: downloaddecompresstestwrappers.DownloadDir,
				OutputDir: downloaddecompresstestwrappers.DecompressFolder,
			},
			Options: &zipperinsfmt.Options{
				Format:           compressformats.Zip,
				CompressionLevel: compresslevels.Best,
			},
			Overwrite: &zipperinsfmt.Overwrite{
				IsForceWrite:       true,
				IsSkipOnExistFiles: false,
			},
			RawConfig: &zipperinsfmt.RawConfig{
				IsMkdirAll: true,
			},
		},
		CopyPath: &pathinsfmt.CopyPath{
			SourceDestinationPlusCompiled: pathinsfmt.SourceDestinationPlusCompiled{
				BaseSourceDestination: pathinsfmt.BaseSourceDestination{
					Source:      downloaddecompresstestwrappers.SourceToDecompressedFolder,
					Destination: downloaddecompresstestwrappers.CopyTo,
				},
			},
			Options: &pathinsfmt.CopyPathOptions{
				IsRecursive: true,
			},
		},
		IsApplyPathsChmod: true,
		BasePathModifiersApply: pathinsfmt.BasePathModifiersApply{
			PathModifiersApply: &pathinsfmt.PathModifiersApply{
				BaseGenericPathsCollection: pathinsfmt.BaseGenericPathsCollection{
					GenericPathsCollection: &pathinsfmt.GenericPathsCollection{
						AllDiffPaths: []pathinsfmt.AllDiffPaths{
							{
								Paths: []string{
									downloaddecompresstestwrappers.CopyTo,
								},
								IsNormalizeApply: true,
							},
						},
					},
				},
				PathModifiers: []pathinsfmt.PathModifier{
					{
						BaseRwxInstructions: chmodins.BaseRwxInstructions{
							RwxInstructions: []chmodins.RwxInstruction{
								{
									RwxOwnerGroupOther: chmodins.RwxOwnerGroupOther{
										Owner: downloaddecompresstestwrappers.RWX,
										Group: downloaddecompresstestwrappers.RWX,
										Other: downloaddecompresstestwrappers.RWX,
									},
								},
							},
						},
					},
				},
			},
		},
	}
}

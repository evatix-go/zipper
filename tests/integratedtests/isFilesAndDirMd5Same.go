package integratedtests

func isFilesAndDirMd5Same(files []string, directory string) bool {
	resultMultipleFiles := multipleFilesMd5(files)
	if resultMultipleFiles.ErrorWrapper.HasError() {
		return false
	}

	resultDirectory := directoryMd5(directory)
	if resultDirectory.ErrorWrapper.HasError() {
		return false
	}

	return resultMultipleFiles.Value == resultDirectory.Value
}

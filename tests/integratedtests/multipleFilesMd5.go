package integratedtests

import (
	"strings"

	"github.com/codingsince1985/checksum"
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/errorwrapper/errdata/errstr"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

func multipleFilesMd5(files []string) *errstr.Result {
	md5Arr := make([]string, constants.Zero, constants.ArbitraryCapacity6)
	for _, file := range files {
		result := singleFileMd5(file)

		if result.ErrorWrapper.HasError() {
			return result
		}

		md5Arr = append(md5Arr, result.Value)
	}

	hashCombinedString := ""
	for _, str := range md5Arr {
		hashCombinedString += str
	}

	md5Hash, err2 := checksum.MD5sumReader(
		strings.NewReader(hashCombinedString))

	if err2 != nil {
		errWp := errnew.Messages.Many(
			errtype.Hash,
			err2.Error(),
			"Hash Failed for",
			hashCombinedString,
			"directoryMd5 -> failed to generate hash for directory",
			"Files:",
			strings.Join(files, constants.NewLineSpaceHyphenSpace))

		return errstr.New.Result.ErrorWrapper(errWp)
	}

	return errstr.New.Result.ValueOnly(md5Hash)
}

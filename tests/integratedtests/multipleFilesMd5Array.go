package integratedtests

import (
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/core/coredata/corestr"
)

func multipleFilesMd5Array(files []string) *corestr.Collection {
	collection := corestr.New.Collection.Cap(constants.Ten)

	for _, file := range files {
		result := singleFileMd5(file)
		result.ErrorWrapper.HandleError()

		collection = collection.Add(result.Value)
	}

	return collection
}

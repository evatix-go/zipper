package integratedtests

import (
	"gitlab.com/evatix-go/pathhelper/pathjoin"

	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func prepareDecompressFromCompress(compress *zipperinsfmt.Compress) *zipperinsfmt.Decompress {
	decompress := &zipperinsfmt.Decompress{
		SourcePath: pathjoin.JoinNormalized(
			compress.Destination.PrefixDir,
			compress.Destination.OutputDir,
		),
		IsClearDir: true,
		Destination: &zipperinsfmt.Destination{
			PrefixDir: TempDirectory,
			OutputDir: compress.Options.Format.String(),
		},
		Overwrite: compress.Overwrite,
		RawConfig: compress.RawConfig,
		Options:   compress.Options,
	}

	return decompress
}

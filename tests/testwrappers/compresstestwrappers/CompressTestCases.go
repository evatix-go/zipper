package compresstestwrappers

var CompressTestCases = []CompressTestWrapper{
	{
		MethodName:   compress,
		PackageName:  zipper,
		CaseHeader:   zipCaseHeader,
		ExpectedData: nil,
		InputData:    getCompressZipArray(),
	},
	{
		MethodName:   compress,
		PackageName:  zipper,
		CaseHeader:   tarCaseHeader,
		ExpectedData: nil,
		InputData:    getCompressTarArray(),
	},
	{
		MethodName:   compress,
		PackageName:  zipper,
		CaseHeader:   tarXZCaseHeader,
		ExpectedData: nil,
		InputData:    getCompressTarXZArray(),
	},
	{
		MethodName:   compress,
		PackageName:  zipper,
		CaseHeader:   tarGZCaseHeader,
		ExpectedData: nil,
		InputData:    getCompressTarGZArray(),
	},
	{
		MethodName:   compress,
		PackageName:  zipper,
		CaseHeader:   tarGZCaseHeader,
		ExpectedData: nil,
		InputData:    getCompressTarBz2Array(),
	},
}

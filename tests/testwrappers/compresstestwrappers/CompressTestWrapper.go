package compresstestwrappers

import (
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

type CompressTestWrapper struct {
	ExpectedData *[]string                 `json:"ExpectedData"`
	InputData    *[]*zipperinsfmt.Compress `json:"InputData"`
	PackageName  string                    `json:"PackageName"`
	MethodName   string                    `json:"MethodName"`
	CaseHeader   string                    `json:"CaseHeader"`
}

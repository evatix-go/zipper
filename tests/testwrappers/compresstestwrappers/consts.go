package compresstestwrappers

const (
	compress         = "Compress"
	zipper           = "zipper"
	zipCaseHeader    = "compress zip test"
	tarCaseHeader    = "compress tar test"
	tarGZCaseHeader  = "compress tar.gz test"
	tarXZCaseHeader  = "compress tar.xz test"
	tarBz2CaseHeader = "compress tar.bz2 test"
)

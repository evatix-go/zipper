package compresstestwrappers

import (
	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/enum/compresslevels"
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

func getCompressZipArray() *[]*zipperinsfmt.Compress {
	compressArray := []*zipperinsfmt.Compress{
		{
			SourcePaths: []string{
				"./assets/json-formats/Compress.json",
				"./assets/json-formats/Decompress.json",
			},
			Destination: &zipperinsfmt.Destination{
				PrefixDir: "./temp",
				OutputDir: "assets1.zip",
			},
			Options: &zipperinsfmt.Options{
				Format:           compressformats.Zip,
				CompressionLevel: compresslevels.Best,
			},
			Overwrite: &zipperinsfmt.Overwrite{
				IsForceWrite:       true,
				IsSkipOnExistFiles: false,
			},
			RawConfig: &zipperinsfmt.RawConfig{
				IsMkdirAll: true,
			},
		},
		{
			SourcePaths: []string{
				"./assets/json-formats/Compress.json",
				"./assets/json-formats/Decompress.json",
			},
			Destination: &zipperinsfmt.Destination{
				PrefixDir: "./temp",
				OutputDir: "assets2.zip",
			},
			Options: &zipperinsfmt.Options{
				Format:           compressformats.Zip,
				CompressionLevel: compresslevels.Fast,
			},
			Overwrite: &zipperinsfmt.Overwrite{
				IsForceWrite:       true,
				IsSkipOnExistFiles: false,
			},
			RawConfig: &zipperinsfmt.RawConfig{
				IsMkdirAll: true,
			},
		},
	}

	return &compressArray
}

func getCompressTarArray() *[]*zipperinsfmt.Compress {
	compressArray := []*zipperinsfmt.Compress{
		{
			SourcePaths: []string{
				"./assets/json-formats/Compress.json",
				"./assets/json-formats/Decompress.json",
			},
			Destination: &zipperinsfmt.Destination{
				PrefixDir: "./temp",
				OutputDir: "assets1.tar",
			},
			Options: &zipperinsfmt.Options{
				Format: compressformats.Tar,
			},
			Overwrite: &zipperinsfmt.Overwrite{
				IsForceWrite:       true,
				IsSkipOnExistFiles: false,
			},
			RawConfig: &zipperinsfmt.RawConfig{
				IsMkdirAll: true,
			},
		},
	}

	return &compressArray
}

func getCompressTarXZArray() *[]*zipperinsfmt.Compress {
	compressArray := []*zipperinsfmt.Compress{
		{
			SourcePaths: []string{
				"./assets/json-formats/Compress.json",
				"./assets/json-formats/Decompress.json",
			},
			Destination: &zipperinsfmt.Destination{
				PrefixDir: "./temp",
				OutputDir: "assets1.tar.xz",
			},
			Options: &zipperinsfmt.Options{
				Format: compressformats.TarXZ,
			},
			Overwrite: &zipperinsfmt.Overwrite{
				IsForceWrite:       true,
				IsSkipOnExistFiles: false,
			},
			RawConfig: &zipperinsfmt.RawConfig{
				IsMkdirAll: true,
			},
		},
	}

	return &compressArray
}

func getCompressTarGZArray() *[]*zipperinsfmt.Compress {
	compressArray := []*zipperinsfmt.Compress{
		{
			SourcePaths: []string{
				"./assets/json-formats/Compress.json",
				"./assets/json-formats/Decompress.json",
			},
			Destination: &zipperinsfmt.Destination{
				PrefixDir: "./temp",
				OutputDir: "assets1.tar.gz",
			},
			Options: &zipperinsfmt.Options{
				Format:           compressformats.TarGZ,
				CompressionLevel: compresslevels.Best,
			},
			Overwrite: &zipperinsfmt.Overwrite{
				IsForceWrite:       true,
				IsSkipOnExistFiles: false,
			},
			RawConfig: &zipperinsfmt.RawConfig{
				IsMkdirAll: true,
			},
		},
	}

	return &compressArray
}

func getCompressTarBz2Array() *[]*zipperinsfmt.Compress {
	compressArray := []*zipperinsfmt.Compress{
		{
			SourcePaths: []string{
				"./assets/json-formats/Compress.json",
				"./assets/json-formats/Decompress.json",
			},
			Destination: &zipperinsfmt.Destination{
				PrefixDir: "./temp",
				OutputDir: "assets1.tar.bz2",
			},
			Options: &zipperinsfmt.Options{
				Format:           compressformats.TarBz2,
				CompressionLevel: compresslevels.Best,
			},
			Overwrite: &zipperinsfmt.Overwrite{
				IsForceWrite:       true,
				IsSkipOnExistFiles: false,
			},
			RawConfig: &zipperinsfmt.RawConfig{
				IsMkdirAll: true,
			},
		},
	}

	return &compressArray
}

package downloaddecompresstestwrappers

import (
	"gitlab.com/evatix-go/core/chmodhelper/chmodins"
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/core/coredata/corestr"
	"gitlab.com/evatix-go/core/enums/stringcompareas"
	"gitlab.com/evatix-go/errorwrapper/errverify"
	"gitlab.com/evatix-go/pathhelper/pathinsfmt"

	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/enum/compresslevels"
	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

var DownloadDecompressVerifierTestCases = []DownloadDecompressVerifierWrapper{
	{
		Header: "Provided Download Url, should download, decompress, copy and modify path without any error",
		Instruction: &zipperinsfmt.DownloadDecompress{
			Download: &pathinsfmt.Download{
				Url:              Url,
				Destination:      DownloadDir,
				FileName:         ZipFileName,
				ParallelRequests: constants.Capacity2,
				MaxRetries:       constants.Capacity2,
				IsCreateDir:      true,
				IsClearDir:       true,
				IsNormalizePath:  true,
				IsSkipOnExist:    false,
			},
			Decompress: &zipperinsfmt.Decompress{
				SourcePath: SourceToZip,
				IsClearDir: true,
				Destination: &zipperinsfmt.Destination{
					PrefixDir: DownloadDir,
					OutputDir: DecompressFolder,
				},
				Options: &zipperinsfmt.Options{
					Format:           compressformats.Zip,
					CompressionLevel: compresslevels.Best,
				},
				Overwrite: &zipperinsfmt.Overwrite{
					IsForceWrite:       true,
					IsSkipOnExistFiles: false,
				},
				RawConfig: &zipperinsfmt.RawConfig{
					IsMkdirAll: true,
				},
			},
			CopyPath: &pathinsfmt.CopyPath{
				SourceDestinationPlusCompiled: pathinsfmt.SourceDestinationPlusCompiled{
					BaseSourceDestination: pathinsfmt.BaseSourceDestination{
						Source:      SourceToDecompressedFolder,
						Destination: CopyTo,
					},
				},
				Options: &pathinsfmt.CopyPathOptions{
					IsRecursive: true,
				},
			},
			IsApplyPathsChmod: true,
			BasePathModifiersApply: pathinsfmt.BasePathModifiersApply{
				PathModifiersApply: &pathinsfmt.PathModifiersApply{
					BaseGenericPathsCollection: pathinsfmt.BaseGenericPathsCollection{
						GenericPathsCollection: &pathinsfmt.GenericPathsCollection{
							AllDiffPaths: []pathinsfmt.AllDiffPaths{
								{
									Paths: []string{
										CopyTo,
									},
									IsNormalizeApply: true,
								},
							},
						},
					},
					PathModifiers: []pathinsfmt.PathModifier{
						{
							BaseRwxInstructions: chmodins.BaseRwxInstructions{
								RwxInstructions: []chmodins.RwxInstruction{
									{
										RwxOwnerGroupOther: chmodins.RwxOwnerGroupOther{
											Owner: RWX,
											Group: RWX,
											Other: RWX,
										},
									},
								},
							},
						},
					},
				},
			},
		},
		Verifier: &errverify.CollectionVerifier{
			Verifier: errverify.Verifier{
				Header: "Given that group (not) exist in the operating system\n" +
					"Should create the os groups successfully without any error",
				FunctionName:             "Test_CreateGroupsErrorVerify",
				VerifyAs:                 stringcompareas.Equal,
				IsCompareEmpty:           false,
				IsVerifyErrorMessageOnly: false,
				IsPrintError:             true,
			},
			ExpectationLines: &corestr.SimpleSlice{
				Items: []string{},
			},
			ErrorLength: 0,
		},
	},
}

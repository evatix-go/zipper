package downloaddecompresstestwrappers

import (
	"gitlab.com/evatix-go/errorwrapper/errverify"

	"gitlab.com/evatix-go/zipper/zipperinsfmt"
)

type DownloadDecompressVerifierWrapper struct {
	Header      string
	Instruction *zipperinsfmt.DownloadDecompress
	Verifier    *errverify.CollectionVerifier
}

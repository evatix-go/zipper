package downloaddecompresstestwrappers

const (
	Url                        = "https://gitlab.com/evatix-go/public-content/uploads/21708626d1899371373cb5b8e70a724e/cimux-ins.zip"
	DownloadDir                = "/home/a/cim-test/"
	ZipFileName                = "cimux.zip"
	SourceToZip                = "/home/a/cim-test/cimux.zip"
	DecompressFolder           = "zipout"
	SourceToDecompressedFolder = "/home/a/cim-test/zipout"
	CopyTo                     = "/home/a/cim-test/zipout_copied"
	RWX                        = "rwx"
	RWXAll                     = "-rwxrwxrwx"
)

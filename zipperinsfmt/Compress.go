package zipperinsfmt

import (
	"encoding/json"

	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

type Compress struct {
	SourcePaths []string     `json:"SourcePaths,omitempty"`
	Destination *Destination `json:"Destination,omitempty"`
	Options     *Options     `json:"Options,omitempty"`
	Overwrite   *Overwrite   `json:"Overwrite,omitempty"`
	RawConfig   *RawConfig   `json:"RawConfig,omitempty"`
}

func (compress *Compress) String() string {
	inBytes, err := json.Marshal(compress)
	if err != nil {
		return errnew.Error.TypeMessages(
			errtype.Marshalling,
			err,
			"Compress -> String -> error in Marshalling").String()
	}

	return string(inBytes) + constants.NewLineUnix
}

func (compress *Compress) IsInvalid() bool {
	return compress == nil ||
		compress.Destination == nil ||
		compress.Options == nil || compress.RawConfig == nil
}

func (compress *Compress) IsValid() bool {
	return !compress.IsInvalid()
}

func (compress *Compress) GetInvalidError() *errorwrapper.Wrapper {
	if compress == nil {
		return errnew.
			Null.
			WithMessage(
				"compress is nil",
				compress)
	}

	if compress.Destination == nil {
		return errnew.Null.
			WithMessage(
				"Compress.Destination is nil",
				compress.Destination)
	}

	if compress.Options == nil {
		return errnew.
			Null.
			WithMessage(
				"compress.options is nil",
				compress.Destination)
	}

	if compress.RawConfig == nil {
		return errnew.
			Null.
			WithMessage(
				"compress.rawConfig is nil",
				compress.RawConfig)
	}

	return nil
}

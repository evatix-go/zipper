package zipperinsfmt

import (
	"encoding/json"

	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

type CompressWithDefaults struct {
	SourcePaths []string     `json:"SourcePaths,omitempty"`
	Destination *Destination `json:"Destination,omitempty"`
}

func (it *CompressWithDefaults) String() string {
	inBytes, err := json.Marshal(it)
	if err != nil {
		return errnew.Error.TypeMessages(
			errtype.Marshalling,
			err,
			"CompressWithDefaults -> String -> error in Marshalling").String()
	}

	return string(inBytes) + constants.NewLineUnix
}

func (it *CompressWithDefaults) IsInvalid() bool {
	return it == nil ||
		it.Destination == nil
}

func (it *CompressWithDefaults) IsValid() bool {
	return !it.IsInvalid()
}

func (it *CompressWithDefaults) GetInvalidError() *errorwrapper.Wrapper {
	if it == nil {
		return errnew.Null.
			WithMessage(
				"CompressWithDefaults is nil",
				it)
	}

	if it.Destination == nil {
		return errnew.Null.
			WithMessage(
				"it.Destination is nil",
				it)
	}

	return nil
}

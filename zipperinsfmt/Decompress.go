package zipperinsfmt

import (
	"encoding/json"

	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

type Decompress struct {
	SourcePath      string       `json:"SourcePath,omitempty"`
	Destination     *Destination `json:"Destination,omitempty"`
	IsClearDir      bool         `json:"IsClearDir,omitempty"`
	Options         *Options     `json:"Options,omitempty"`
	Overwrite       *Overwrite   `json:"Overwrite,omitempty"`
	RawConfig       *RawConfig   `json:"RawConfig,omitempty"`
	ExtractSpecific []string     `json:"ExtractSpecific,omitempty"` // Todo :: Not Supported
}

func (it *Decompress) String() string {
	inBytes, err := json.Marshal(it)
	if err != nil {
		return errnew.Error.TypeMessages(
			errtype.Marshalling,
			err,
			"Decompress -> String -> error in Marshalling").String()
	}

	return string(inBytes) + constants.NewLineUnix
}

func (it *Decompress) IsInvalid() bool {
	return it == nil ||
		it.Destination == nil ||
		it.Options == nil
}

func (it *Decompress) IsValid() bool {
	return !it.IsInvalid()
}

func (it *Decompress) GetInvalidError() *errorwrapper.Wrapper {
	if it == nil {
		errnew.Null.Simple(it)
	}

	if it.Destination == nil {
		return errnew.Null.Simple(it.Destination)
	}

	if it.Options == nil {
		return errnew.Null.Simple(it.Options)
	}

	return nil
}

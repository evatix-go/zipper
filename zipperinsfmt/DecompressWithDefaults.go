package zipperinsfmt

import (
	"encoding/json"

	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

type DecompressWithDefaults struct {
	SourcePath  string       `json:"SourcePath,omitempty"`
	Destination *Destination `json:"Destination,omitempty"`
}

func (it *DecompressWithDefaults) String() string {
	inBytes, err := json.Marshal(it)
	if err != nil {
		return errnew.Error.TypeMessages(
			errtype.Marshalling,
			err,
			"DecompressWithDefaults -> String -> error in Marshalling").
			String()
	}

	return string(inBytes) + constants.NewLineUnix
}

func (it *DecompressWithDefaults) IsInvalid() bool {
	return it == nil ||
		it.Destination == nil
}

func (it *DecompressWithDefaults) IsValid() bool {
	return !it.IsInvalid()
}

func (it *DecompressWithDefaults) GetInvalidError() *errorwrapper.Wrapper {
	if it == nil {
		return errnew.Null.
			WithMessage(
				"DecompressWithDefaults is nil",
				it)
	}

	if it.Destination == nil {
		return errnew.Null.
			WithMessage(
				"DecompressWithDefaults.Destination is nil",
				it)
	}

	return nil
}

package zipperinsfmt

type Destination struct {
	PrefixDir string `json:"PrefixDir,omitempty"`
	OutputDir string `json:"OutputDir,omitempty"`
}

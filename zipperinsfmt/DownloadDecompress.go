package zipperinsfmt

import (
	"gitlab.com/evatix-go/pathhelper/pathinsfmt"
)

// DownloadDecompress
// Steps performed in order
//  - Download
//  - Decompress
//  - CopyPath (copy move)
//  - pathinsfmt.BasePathModifiersApply (DownloadDecompress. PathModifiersApply)
type DownloadDecompress struct {
	pathinsfmt.BasePathModifiersApply
	IsApplyPathsChmod bool                 `json:"IsApplyPathsChmod"`
	Download          *pathinsfmt.Download `json:"Download,omitempty"`
	Decompress        *Decompress          `json:"Decompress,omitempty"`
	CopyPath          *pathinsfmt.CopyPath `json:"CopyPath,omitempty"`
}

func (it *DownloadDecompress) HasDownload() bool {
	return it != nil && it.Download != nil
}

func (it *DownloadDecompress) HasDecompress() bool {
	return it != nil && it.Decompress != nil
}

func (it *DownloadDecompress) HasCopyPath() bool {
	return it != nil && it.CopyPath != nil
}

func (it *DownloadDecompress) HasPathModifiersApply() bool {
	return it != nil && it.IsApplyPathsChmod && it.PathModifiersApply != nil
}

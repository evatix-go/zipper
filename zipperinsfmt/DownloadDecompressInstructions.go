package zipperinsfmt

type DownloadDecompressInstructions struct {
	Instructions []DownloadDecompress `json:"Instructions,omitempty"`
}

func (it *DownloadDecompressInstructions) Length() int {
	if it == nil {
		return 0
	}

	return len(it.Instructions)
}

func (it *DownloadDecompressInstructions) IsEmpty() bool {
	return it.Length() == 0
}

func (it *DownloadDecompressInstructions) HasAnyItem() bool {
	return it.Length() > 0
}

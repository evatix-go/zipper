package zipperinsfmt

import (
	"encoding/json"

	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

type ExtractMultipleFiles struct {
	SourcePaths []string     `json:"SourcePaths,omitempty"`
	SourcePath  string       `json:"SourcePath,omitempty"`
	Destination *Destination `json:"Destination,omitempty"`
	Options     *Options     `json:"Options"`
	IsClearDir  bool         `json:"IsClearDir"`
	Overwrite   *Overwrite   `json:"Overwrite"`
	RawConfig   *RawConfig   `json:"RawConfig"`
}

func (it *ExtractMultipleFiles) String() string {
	inBytes, err := json.Marshal(it)
	if err != nil {
		return errnew.Error.TypeMessages(
			errtype.Marshalling,
			err,
			"ExtractMultipleFiles -> String -> error in Marshalling").String()
	}

	return string(inBytes) + constants.NewLineUnix
}

func (it *ExtractMultipleFiles) IsInvalid() bool {
	return it == nil ||
		it.Destination == nil ||
		it.Options == nil || it.RawConfig == nil
}

func (it *ExtractMultipleFiles) IsValid() bool {
	return !it.IsInvalid()
}

func (it *ExtractMultipleFiles) GetInvalidError() *errorwrapper.Wrapper {
	if it == nil {
		return errnew.Null.
			WithMessage(
				"it is nil",
				it)
	}

	if it.Destination == nil {
		return errnew.Null.
			WithMessage(
				"it.Destination is nil",
				it.Destination)
	}

	if it.Options == nil {
		return errnew.Null.
			WithMessage(
				"it.Options is nil",
				it.Options)
	}

	if it.RawConfig == nil {
		return errnew.Null.
			WithMessage(
				"it.RawConfig is nil",
				it.RawConfig)
	}

	return nil
}

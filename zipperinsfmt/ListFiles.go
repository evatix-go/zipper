package zipperinsfmt

import (
	"encoding/json"

	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

type ListFiles struct {
	SourcePath string   `json:"SourcePath,omitempty"`
	Options    *Options `json:"Options"`
}

func (listFiles *ListFiles) String() string {
	inBytes, err := json.Marshal(listFiles)
	if err != nil {
		return errnew.Error.TypeMessages(
			errtype.Marshalling,
			err,
			"ListFiles -> String -> error in Marshalling").String()
	}

	return string(inBytes) + constants.NewLineUnix
}

func (listFiles *ListFiles) IsInvalid() bool {
	return listFiles == nil ||
		listFiles.Options == nil
}

func (listFiles *ListFiles) IsValid() bool {
	return !listFiles.IsInvalid()
}

func (listFiles *ListFiles) GetInvalidError() *errorwrapper.Wrapper {
	if listFiles == nil {
		return errnew.Null.
			WithMessage(
				"listFiles is nil",
				listFiles)
	}

	if listFiles.Options == nil {
		return errnew.Null.
			WithMessage(
				"listFiles.Options is nil",
				listFiles.Options)
	}

	return nil
}

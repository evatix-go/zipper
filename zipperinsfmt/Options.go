package zipperinsfmt

import (
	"gitlab.com/evatix-go/enum/compressformats"
	"gitlab.com/evatix-go/enum/compresslevels"
)

type Options struct {
	CompressionLevel compresslevels.Variant  `json:"CompressionLevel,omitempty"`
	Format           compressformats.Variant `json:"Format,omitempty"`
}

package zipperinsfmt

type Overwrite struct {
	IsForceWrite       bool `json:"IsForceWrite"`
	IsSkipOnExistFiles bool `json:"IsSkipOnExistFiles"`
}

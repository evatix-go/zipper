package zipperinsfmt

type RawConfig struct {
	IsContinueOnError        bool `json:"IsContinueOnError,omitempty"`
	IsSelectiveCompression   bool `json:"IsSelectiveCompression,omitempty"`
	IsMkdirAll               bool `json:"IsMkdirAll,omitempty"`
	IsImplicitTopLevelFolder bool `json:"IsImplicitTopLevelFolder,omitempty"`
	StripComponents          int  `json:"StripComponents,omitempty"`
	IsSingleThreadedCompress bool `json:"IsSingleThreadedCompress,omitempty"`
}
